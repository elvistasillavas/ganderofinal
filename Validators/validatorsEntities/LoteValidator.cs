﻿using Ganadero.Model.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validators.validatorsEntities
{
   public  class LoteValidator
    {
       public virtual bool Pass(Lote Lote)
       {
           if (Lote.NroLote==null)
               return false;
           if ((Lote.FechaDeVencimiento==null))
               return false;
           return true;
       }
    }
}
