﻿using Ganadero.Model.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validators.validatorsEntities
{
    public  class ProductoValidator
    {
        public virtual bool Pass(Producto producto)
        {
            if (String.IsNullOrEmpty(producto.NombreProducto))
                return false;
            if (String.IsNullOrEmpty(producto.Marca))
                return false;
            return true;
        }
    }
}
