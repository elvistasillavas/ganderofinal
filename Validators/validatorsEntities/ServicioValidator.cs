﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ganadero.Model.Entidades;

namespace Validators.validatorsEntities
{
    public class ServicioValidator
    {
        public virtual bool Pass(Servicio servicio)
        {
            if (String.IsNullOrEmpty(servicio.NombreServicio))
                return false;
            //if (servicio.Id_servicio == Int32.Parse(""))
            //    return false;
            //if (servicio.Precio == decimal.Parse(""))
            //    return false;
            if (String.IsNullOrEmpty(servicio.Descripcion))
                return false;
            return true;
        }
    }
}
