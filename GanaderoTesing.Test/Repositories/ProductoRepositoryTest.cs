﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ganadero.Model.Entidades;
using Ganadero.Repository.Repositories;
using NUnit.Framework;
using GenerateDB;
using Ganadero.Repository;

namespace GanaderoTesing.Test.Repositories
{
     [TestFixture]
    public class ProductoRepositoryTest
    { 
        [Test]
        public void AllReturnListProducts()
        {
            var repo = new ProductoRepository(new DBSystemContext());

            var result = repo.All();

            Assert.IsInstanceOf(typeof(List<Producto>), result);
            Assert.AreEqual(5, result.Count());

        }
    }
}
