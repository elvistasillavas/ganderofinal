﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ganadero.Model.Entidades;

namespace Ganadero.Interfaces.Interfaces
{
   public  interface InterfaceEntidadCliente
    {
        entidadCliente Find(int id);
        List<entidadCliente> All();
    }
}
