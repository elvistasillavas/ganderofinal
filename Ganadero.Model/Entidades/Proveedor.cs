﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ganadero.Model.Entidades
{
    public class Proveedor
    {
         public int IdProveedor { get; set; }
      
        public string Ruc { get; set; }
       
        public string Nombre { get; set; }
      
      // [Range(8, 10, ErrorMessage = "Ingrese un número celular correcto")]
        public string Telefono { get; set; }
       
        public string Ciudad { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "El email no tiene el formato correcto")]
        public string Correo { get; set; }
      [Required]
        public string PaginaWeb { get; set; }

  
    }
}
