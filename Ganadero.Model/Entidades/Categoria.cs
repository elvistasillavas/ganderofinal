﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Ganadero.Model.Entidades
{
    public class Categoria
    {
        public Int32 IdCategoria { get; set; }
    
        public string Descripcion { get; set; }
        public virtual List<Producto> producto { get; set; }

    }
}