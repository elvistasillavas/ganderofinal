﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;

namespace Ganadero.Model.Entidades
{
    public class Lote
    {
        public Int32 IdLote { get; set; }
       
        public DateTime FechaDeVencimiento { get; set; }
     
        public Int32 NroLote { get; set; }
       
        public Int32 StockInicial { get; set; }
       
        public int idProducto { get; set; }
        public Producto producto { get; set; }

        //proveedor
   
        public int idProveedor { get; set; }
        public Proveedor Proveedor{ get; set; }
    }
}