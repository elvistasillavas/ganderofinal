﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ElGanadero.Model;
using Ganadero.Interfaces.Interfaces;
using Ganadero.Model.Entidades;
using Validators.validatorsEntities;

 using  ElGanadero;


namespace ElGanadero.Controllers
{
    public class VentaController : Controller
    {

        GanaderoEntities4 context = null;
     
        private InterfaceProducto productorepo;
        private ventavalidator validator;
        private InterfaceVenta ventarepo;
        private InterfaceDetalle detallerepo;
         private InterfaceEntidadCliente entidadClirepo;
        
        
        public VentaController(ventavalidator validator, InterfaceProducto productorepo, InterfaceVenta ventarepo, InterfaceEntidadCliente entidadClirepo,InterfaceDetalle detallerepo)
        {
            this.productorepo = productorepo;
            this.ventarepo = ventarepo;
            this.validator = validator;
            this.detallerepo = detallerepo;
            this.entidadClirepo=entidadClirepo;
        }

        public ActionResult Index( int query=1)
        {
           
            
                var dato = ventarepo.ByQueryAll(query);
                return View("Index", dato);
          
          
        }



       [HttpGet]
        public PartialViewResult Detalle(int id)
        {
            context = new GanaderoEntities4();
             var  detalle=context.DetalleVentas.Where(x => x.IdVentas == id).ToList();
            return PartialView(detalle);
        }



        [HttpGet]
        public ViewResult Create(string query="")
        {
            var cliente = entidadClirepo.All();
            ViewData["identiti"] = new SelectList(cliente, "id", "nombre");

            var dato = productorepo.ByQueryAll(query);
            return View("Create",dato);
        }
        [HttpGet]
        public PartialViewResult addproducto(int id, int index)
        {
            var producto = productorepo.Find(id);
            ViewBag.Index = index;
            return PartialView(producto);
        }


        [HttpPost]

        public ActionResult Create(Ganadero.Model.Entidades.Venta venta)
        {

            if (validator.Pass(venta))
            {

               ventarepo.Store(venta);

                TempData["UpdateSuccess"] = "la venta se a guardado correctamente";
                return RedirectToAction("index");
            }

            return View("create", venta);
        }


        [HttpGet]
        public ViewResult anular(int id)
        {
            var data = ventarepo.Find(id);
          
            return View("anular", data);
        }

        [HttpPost]
        public ActionResult anular(Ganadero.Model.Entidades.Venta venta)
        {
            ventarepo.Update(venta);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Index");
        }

    }
      

    
}
