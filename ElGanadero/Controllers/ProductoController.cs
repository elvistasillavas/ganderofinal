﻿using ElGanadero.Model;
using Ganadero.Interfaces.Interfaces;
using Ganadero.Model.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Validators.validatorsEntities;

namespace ElGanadero.Controllers
{
    public class ProductoController : Controller
    {

        private InterfaceProducto repository;
        private ProductoValidator validator;
        private InterfaceCategoria categoriarepo;
      


        public ProductoController(InterfaceProducto repository, ProductoValidator validator, InterfaceCategoria categoriarepo)
        {
            this.repository = repository;
            this.validator = validator;
            this.categoriarepo = categoriarepo;
            

        }

        [HttpGet]
        public ViewResult Inicio(string query = "")
        {
            var dato = repository.ByQueryAll(query);
            return View("Inicio", dato);
        }


        [HttpGet]
        public ViewResult Create()
        {
            var categoria = categoriarepo.All();
            ViewData["IDCategoria"] = new SelectList(categoria, "IdCategoria", "Descripcion");

            return View("Create");
        }

      

        [HttpPost]
        public ActionResult Create(Ganadero.Model.Entidades.Producto producto)
        {
           
            if (validator.Pass(producto))
            {
                repository.Store(producto);

                TempData["UpdateSuccess"] = " el producto se a guardado correctamente";
                return RedirectToAction("Inicio");
            }

            return View("Create", producto);
        }

        [HttpGet]
        public ViewResult Editar(int id)
        {
            var data = repository.Find(id);
            var categoria = categoriarepo.All();
            ViewData["IDCategoria"] = new SelectList(categoria, "IdCategoria", "Descripcion",data.IDCategoria);
           
          
            return View("Editar", data);
        }

        [HttpPost]
        public ActionResult Editar(Ganadero.Model.Entidades.Producto producto)
        {
            repository.Update(producto);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Inicio");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Inicio");
        }
    }
    
}
