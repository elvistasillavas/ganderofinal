﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElGanadero.Models
{
    public class OrdenDeCompra
    {
        public Guid IdOrdenCompra { get; set; }
        public Int32 Cantidad { get; set; }
        public Int32 Id_producto { get; set; }
        public decimal Precio { get; set; }
        public Producto Producto { get; set; }
    }
}