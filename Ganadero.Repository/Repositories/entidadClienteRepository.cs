﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ganadero.Model.Entidades;
using Ganadero.Interfaces.Interfaces;
using GenerateDB;

namespace Ganadero.Repository.Repositories
{
    public class entidadClienteRepository:InterfaceEntidadCliente
    {
         DBSystemContext entities;

         public entidadClienteRepository(DBSystemContext entities)
        {
            this.entities = entities;
        }

        public entidadCliente Find(int id)
        {
            var result = from p in entities.entidadCliente where p.id == id select p;
            return result.FirstOrDefault();
        }

        public List<entidadCliente> All()
        {
            using (DBSystemContext context = new DBSystemContext())
            {
                return (List<entidadCliente>)context.Set<entidadCliente>().ToList();
            }
        }
    }
}
